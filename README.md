Simple Module to add "No cache" Control to pages of Drupal.
Normally Drupal allows visiting admin pages offline and through history tabs even after logout.
After installing this module, after logging out this module will always refresh and hit new request for pages. This will assure that admin pages are not visible through back button, offline or throgh history.