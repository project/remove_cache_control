<?php
namespace Drupal\remove_cache_control\EventSubscriber;
 
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
/**
 * Event subscriber to remove Cache-Control from header.
 */
class CacheControlEventSubscriber implements EventSubscriberInterface {
 
  public function onRespond(FilterResponseEvent $event) {
    // Remove Cache-Control from response headers.
    $response = $event->getResponse();
    $response->headers->set('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }
}